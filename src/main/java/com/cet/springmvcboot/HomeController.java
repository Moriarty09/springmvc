package com.cet.springmvcboot;

import java.util.Locale;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	@RequestMapping("display")
	public String home() {
		return "index";
	}
	
	@RequestMapping("add")
//	Without RequestParam 
//	public String add(HttpServletRequest req) {
//		
//		int i = Integer.parseInt(req.getParameter("num1"));
//		int j = Integer.parseInt(req.getParameter("num2"));
//		
//		int num3 = i + j;
//		
//		HttpSession session = req.getSession();
//		session.setAttribute("num3", num3);g
//		
//		return "output.jsp"; 
//	} 
	
	//With RequestParam
//	public String add(@RequestParam("num1")int i, @RequestParam("num2")int j, HttpServletRequest req) {
//		i = Integer.parseInt(req.getParameter("num1"));
//		j = Integer.parseInt(req.getParameter("num2"));
//		
//		int num3 = i + j;
//		
//		HttpSession session = req.getSession();
//		session.setAttribute("num3", num3);
//		
//		return "output.jsp";
//	}
	
	//With Model&View 
//		public ModelAndView add(@RequestParam("num1")int i, @RequestParam("num2")int j) {
//			ModelAndView mv = new ModelAndView();
//			mv.setViewName("output");
//			
//			int num3 = i + j;
//			
//			
//			mv.addObject("num3",num3);
//			return mv;
//		}
	
	// With Model & Model Map
	// Model m can also be replaced as (ModelMap m) if we want to have data in the format of map 
	public String add(@RequestParam("num1")int i, @RequestParam("num2")int j, Model m) {
//		ModelAndView mv = new ModelAndView();
//		mv.setViewName("output");
		
		int num3 = i + j;
		
		
		//mv.addObject("num3",num3);
		m.addAttribute("num3",num3);
		
		return "output";
	}

} 
